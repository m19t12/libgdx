package com.gamestudio24.martianrun.enums;

/**
 * Created by Manolis on 1/10/2015.
 */
public enum UserDataType {

    GROUND,
    RUNNER,
    ENEMY
}
