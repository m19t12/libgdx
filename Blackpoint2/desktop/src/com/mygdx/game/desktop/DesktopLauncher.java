package com.mygdx.game.desktop;


import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.Blackpoint2;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = Blackpoint2.TITLE + " v " + Blackpoint2.VERSION;
		config.vSyncEnabled = true;
		config.useGL30 = true;
		config.width = 800;
		config.height = 480;
		config.addIcon("img/particle.png", Files.FileType.Internal);

		new LwjglApplication(new Blackpoint2(), config);
	}
}
