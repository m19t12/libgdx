package com.mygdx.game;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.mygdx.game.entites.AnnotationAssets;
import com.mygdx.game.entites.Assets;
import com.mygdx.game.screens.AnimationTutorial;
import com.mygdx.game.screens.Credits;
import com.mygdx.game.screens.GameScreen;
import com.mygdx.game.screens.LevelMenu;
import com.mygdx.game.screens.ParticleEffects;
import com.mygdx.game.screens.Play;
import com.mygdx.game.screens.Play2;
import com.mygdx.game.screens.PoolingTutorial;
import com.mygdx.game.screens.SortedSpriteTutorial;
import com.mygdx.game.screens.Splash;
import com.mygdx.game.screens.WaypointsTutorial;
import com.mygdx.game.screens.XmlParsingTutorial;

import net.dermetfan.gdx.assets.AnnotationAssetManager;

public class Blackpoint2 extends Game{

	public static final String TITLE = "Blackpoint2", VERSION = "0.0.0.0.reallyEarly";

	@Override
	public void create () {
		AnnotationAssets.manager.load(AnnotationAssets.class);
		AnnotationAssets.manager.finishLoading();
		//loading screen

		setScreen(new Splash());
	}

	@Override
	public void dispose () {
		super.dispose();
	}

	@Override
	public void render () {
		super.render();
	}

	@Override
	public void resize(int width, int height){
		super.resize(width,height);
	}

	@Override
	public void pause(){
		super.pause();
	}

	@Override
	public void resume(){
		AnnotationAssets.dispose();
		//Assets.dispose();
		super.resume();
	}


}
