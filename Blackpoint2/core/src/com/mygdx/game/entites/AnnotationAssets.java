package com.mygdx.game.entites;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.graphics.Texture;

import net.dermetfan.gdx.assets.AnnotationAssetManager;

/**
 * Created by Manolis on 1/12/2015.
 */
public class AnnotationAssets {

    public static final AnnotationAssetManager manager = new AnnotationAssetManager();

    @AnnotationAssetManager.Asset(Texture.class)
    public static final String algorithm = "img/drop.png",algorithm2 = "img/drop.png",algorithm3 = "img/drop.png", algorithm4 = "img/drop.png";

    @AnnotationAssetManager.Asset
    public static final AssetDescriptor<Texture> algorithm5 = new AssetDescriptor<Texture>("img/drop.png",Texture.class);



    public static void dispose(){
        manager.dispose();
    }
}
