package com.mygdx.game.entites;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by Manolis on 1/12/2015.
 */
public class Assets {

    public static final AssetManager manager = new AssetManager();

    public static final String algorithm = "img/drop.png";
    public static final String algorithm2 = "img/drop.png";
    public static final String algorithm3 = "img/drop.png";
    public static final String algorithm4 = "img/drop.png";
    public static final AssetDescriptor<Texture> algorithm5 = new AssetDescriptor<Texture>("img/drop.png",Texture.class);

    public static void load(){
        manager.load(algorithm, Texture.class);
        manager.load(algorithm2, Texture.class);
        manager.load(algorithm3, Texture.class);
        manager.load(algorithm4, Texture.class);
        manager.load(algorithm5);
    }

    public static void dispose(){
        manager.dispose();
    }
}
