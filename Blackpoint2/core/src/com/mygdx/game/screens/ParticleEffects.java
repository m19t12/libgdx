package com.mygdx.game.screens;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.InputController;
import com.mygdx.game.InputListener;

/**
 * Created by Manolis on 26/11/2015.
 */
public class ParticleEffects implements Screen{

    private SpriteBatch batch;
    private ParticleEffect prototype;
    private ParticleEffectPool pool;
    private Array<ParticleEffectPool.PooledEffect> effects;
    private Skin skin;

    @Override
    public void show() {
        batch = new SpriteBatch();

        skin = new Skin(Gdx.files.internal("ui/menuSkin.json"), new TextureAtlas("ui/atlas.pack"));

        prototype = new ParticleEffect();
        prototype.load(Gdx.files.internal("particles/explosion.p"), Gdx.files.internal("img"));
        prototype.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        prototype.start();
        //prototype.findEmitter("smoke").setContinuous(true);

        pool = new ParticleEffectPool(prototype, 0, 25);
        effects = new Array<ParticleEffectPool.PooledEffect>();

        Gdx.input.setInputProcessor(new InputListener() {
            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                ParticleEffectPool.PooledEffect effect = pool.obtain();
                effect.setPosition(screenX, -screenY + Gdx.graphics.getHeight());
                effects.add(effect);
                return true;
            }
        });
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        batch.begin();
        for (ParticleEffectPool.PooledEffect effect : effects) {
            effect.draw(batch, delta);
            if (effect.isComplete()) {
                effects.removeValue(effect, true);
                effect.free();
            }
        }
        //prototype.draw(batch, delta);
        batch.end();

        Gdx.app.log("pool stats", "active: " + effects.size + " | free: " + pool.getFree() + "/" + pool.max + "| record: " + pool.peak);
        /*if(prototype.isComplete())
            prototype.reset();*/
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        batch.dispose();
        prototype.dispose();
    }
}
