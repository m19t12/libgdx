package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.AnimatedSprite;

/**
 * Created by Manolis on 25/11/2015.
 */
public class AnimationTutorial implements Screen {

    private SpriteBatch batch;
    private AnimatedSprite animatedSprite;
    //private float time;

    @Override
    public void show() {
        batch = new SpriteBatch();

        Animation animation = new Animation(1 / 3f, new TextureRegion(new Texture("splash.png")), new TextureRegion(new Texture("img/luigi_front.png")), new TextureRegion(new Texture("img/luigi_side.png")));
        animation.setPlayMode(Animation.PlayMode.LOOP);

        animatedSprite = new AnimatedSprite(animation);
        //keep size of first frame
        //animatedSprite.setKeepSize(true);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        batch.begin();
        animatedSprite.draw(batch);
        //batch.draw(animation.getKeyFrame(time += delta), 0, 0);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
