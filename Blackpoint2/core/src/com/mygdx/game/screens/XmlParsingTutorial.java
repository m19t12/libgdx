package com.mygdx.game.screens;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;

public class XmlParsingTutorial implements Screen {

    private SpriteBatch batch;
    private Sprite sprite;

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        sprite.draw(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        batch = new SpriteBatch();

        try {
            Element root = new XmlReader().parse(Gdx.files.internal("sprite.xml"));

            sprite = new Sprite(new Texture(root.get("texture")));

            sprite.setPosition(root.getFloat("x"), root.getFloat("y"));
            sprite.setSize(root.getFloat("width"), root.getFloat("height"));

            Element origin = root.getChildByName("origin");
            sprite.setOrigin(origin.getFloat("x"), origin.getFloat("y"));

            Element scale = root.getChildByName("scale"), color = root.getChildByName("color");
            sprite.setScale(scale.getFloat("x"), scale.getFloat("y"));
            sprite.setColor(color.getFloat("r"), color.getFloat("g"), color.getFloat("b"), color.getFloat("a"));

            sprite.setRotation(root.getFloat("rotation"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        batch.dispose();
        sprite.getTexture().dispose();
    }

}