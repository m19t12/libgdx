package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.mygdx.game.InputListener;

/**
 * Created by Manolis on 26/11/2015.
 */
public class PoolingTutorial implements Screen {

    private SpriteBatch batch;
    private Array<Water> drops;
    private Texture dropTexture;
    private SwimmingPool pool;


    @Override
    public void show() {
        batch = new SpriteBatch();
        drops = new Array<Water>();
        dropTexture = new Texture("img/drop.png");
        pool = new SwimmingPool();

        Gdx.input.setInputProcessor(new InputListener() {
            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                Water drop = pool.obtain();
                drop.x = screenX;
                drop.y = -screenY + Gdx.graphics.getHeight();
                drops.add(drop);
                return true;
            }
        });
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        batch.begin();
        for (Water drop : drops)
            drop.draw(batch);
        batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        batch.dispose();
        dropTexture.dispose();
    }

    private class SwimmingPool extends Pool<Water> {
        @Override
        protected Water newObject() {
            return new Water(dropTexture, 0, 0, 90, 110);
        }
    }

    private class Water implements Pool.Poolable{

        private Texture texture;

        private float x, y, width, height, life = 2;

        public Water(Texture texture, float x, float y, float width, float height) {
            this.texture = texture;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public void draw(SpriteBatch batch) {
            batch.draw(texture, x, y, width, height);
            life -= Gdx.graphics.getDeltaTime();
            if (life <= 0) {
                drops.removeValue(this, true);
                pool.free(this);
            }
        }

        @Override
        public void reset() {
            x = 0;
            y = 0;
            width = 50;
            height = 70;
            life = 2;
        }
    }

}
