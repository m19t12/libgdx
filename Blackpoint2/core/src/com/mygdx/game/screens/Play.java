package com.mygdx.game.screens;


import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.LevelGenerator;
import com.mygdx.game.entites.Car;
import com.mygdx.game.entites.Player;


/**
 * Created by Manolis on 30/10/2015.
 */

public class Play implements Screen {

    private World world;
    private Box2DDebugRenderer debugRenderer;
    private OrthographicCamera camera;
    private SpriteBatch batch;

    private final float TIMESTEP = 1 / 60f;
    private final int VELOCITYITERATIONS = 8, POSITIONITERETION = 3;

    private LevelGenerator levelGenerator;
    private Car car;
    private Player player;

    private Vector3 bottomLeft, bottomRight;

    private Array<Body> tmpBodies = new Array<Body>();

    @Override
    public void show() {
        if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
            Gdx.graphics.setDisplayMode((int) (Gdx.graphics.getHeight() / 1.5f), Gdx.graphics.getHeight() / 2, false);
        }
        world = new World(new Vector2(0, -9.81f), true);
        debugRenderer = new Box2DDebugRenderer();
        batch = new SpriteBatch();

        camera = new OrthographicCamera(Gdx.graphics.getWidth() / 25, Gdx.graphics.getHeight() / 25);
        //camera.position.set(camera.viewportHeight / 2, camera.viewportHeight / 2, 0);
        player = new Player(world, 0, 1, 1);
        world.setContactFilter(player);
        world.setContactListener(player);

        BodyDef bodyDef = new BodyDef();
        FixtureDef fixtureDef = new FixtureDef(), wheelFixtureDef = new FixtureDef();

        /*//Car
        fixtureDef.density = 5;
        fixtureDef.friction = .4f;
        fixtureDef.restitution = .3f;

        wheelFixtureDef.density = fixtureDef.density * 1.5f;
        wheelFixtureDef.friction = 50;
        wheelFixtureDef.restitution = .4f;

        car = new Car(world,fixtureDef,wheelFixtureDef,0,3,3,1.5f);*/

        //Gdx.input.setOnscreenKeyboardVisible(true);

        Gdx.input.setInputProcessor(new InputMultiplexer(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                switch (keycode) {
                    case Input.Keys.ESCAPE:
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelMenu());
                        break;
                }
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                camera.zoom += amount / 25f;
                return true;
            }
        }, player));

        // Ground
        //body definition
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(0, 0);

        // ground shape
        ChainShape groundShape = new ChainShape();
        bottomLeft = new Vector3(0, Gdx.graphics.getHeight(), 0);
        bottomRight = new Vector3(Gdx.graphics.getWidth(), bottomLeft.y, 0);

        camera.unproject(bottomLeft);
        camera.unproject(bottomRight);

        groundShape.createChain(new float[]{bottomLeft.x, bottomLeft.y, bottomRight.x, bottomRight.y});

        //fixture definition
        fixtureDef.shape = groundShape;
        fixtureDef.friction = .5f;
        fixtureDef.restitution = 0;

        Body ground = world.createBody(bodyDef);
        ground.createFixture(fixtureDef);

        groundShape.dispose();

        levelGenerator = new LevelGenerator(ground, bottomLeft.x, bottomRight.x, player.WIDTH / 2, player.HEIGHT * 3, player.WIDTH * 1.5f, player.WIDTH * 3.5f, player.WIDTH / 3, 20 * MathUtils.degRad);
    }

    @Override
    public void render(float delta) {
        // ShaderProgram.pedantic = false;
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        if (player.getBody().getPosition().x < bottomLeft.x) {
            player.getBody().setTransform(bottomRight.x, player.getBody().getPosition().y, player.getBody().getAngle());
        } else if (player.getBody().getPosition().x > bottomRight.x) {
            player.getBody().setTransform(bottomLeft.x, player.getBody().getPosition().y, player.getBody().getAngle());
        }

        player.update();
        world.step(TIMESTEP, VELOCITYITERATIONS, POSITIONITERETION);

        camera.position.y = player.getBody().getPosition().y > camera.position.y ? player.getBody().getPosition().y : camera.position.y;
        camera.update();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        world.getBodies(tmpBodies);
        for (Body body : tmpBodies)
            if (body.getUserData() != null && body.getUserData() instanceof Sprite) {
                Sprite sprite = (Sprite) body.getUserData();
                sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2, body.getPosition().y - sprite.getHeight() / 2);
                sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
                sprite.draw(batch);
            }
        batch.end();

        debugRenderer.render(world, camera.combined);

        // top of the screen
        levelGenerator.generate(camera.position.y + camera.viewportHeight /2 );
    }

    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width / 25;
        camera.viewportHeight = height / 25;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        world.dispose();
        debugRenderer.dispose();
    }
}
