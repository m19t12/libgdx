package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.entites.AnnotationAssets;
import com.mygdx.game.entites.Assets;

/**
 * Created by Manolis on 1/12/2015.
 */
public class GameScreen extends ScreenAdapter {

    private SpriteBatch batch = new SpriteBatch();
    private Sprite sprite;

    @Override
    public void show() {
        //sprite = new Sprite(Assets.manager.get(Assets.algorithm, Texture.class));
        sprite = new Sprite(AnnotationAssets.manager.get(AnnotationAssets.algorithm, Texture.class));
        sprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        batch.begin();
        sprite.draw(batch);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
