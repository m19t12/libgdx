package com.mygdx.game.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Manolis on 12/10/2015.
 */
public class LevelMenu implements Screen {

    private Stage stage;
    private Table table;
    private Skin skin;

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
        // workaround because Table.setTransform(boolean transform) is not working
        table.invalidateHierarchy();

    }

    @Override
    public void show() {
        stage = new Stage();

        Gdx.input.setInputProcessor(stage);

        skin = new Skin(Gdx.files.internal("ui/menuSkin.json"), new TextureAtlas("ui/atlas.pack"));

        table = new Table(skin);
        table.setFillParent(true);


        final List<String> list = new List<String>(skin);
        list.setItems(new String[]{"Play", "Pooling Tutorial", "Particle Effects", "Animation Tutorial", "Credits", "Path Tutorial", "Sort Tutorial"});

        ScrollPane scrollPane = new ScrollPane(list, skin);

        TextButton play = new TextButton("PLAY", skin, "small");
        play.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                switch (list.getSelectedIndex()) {
                    case 0:
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new Play());
                        break;
                    case 1:
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new PoolingTutorial());
                        break;
                    case 2:
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new ParticleEffects());
                        break;
                    case 3:
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new AnimationTutorial());
                        break;
                    case 4:
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new Credits());
                        break;
                    case 5:
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new WaypointsTutorial());
                        break;
                    case 6:
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new SortedSpriteTutorial());
                        break;
                }
            }
        });
        play.pad(15);

        TextButton back = new TextButton("BACK", skin);
        back.addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                stage.addAction(sequence(moveTo(0, stage.getHeight(), .5f), run(new Runnable() {

                    @Override
                    public void run() {
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenu());
                    }
                })));
            }
        });
        back.pad(10);

        table.add(new Label("SELECT LEVEL", skin, "small")).colspan(3).expandX().spaceBottom(50).row();
        table.add(scrollPane).uniformX().expandY().top().left();
        table.add(play).uniformX();
        table.add(back).uniformX().bottom().right();

        stage.addActor(table);

        stage.addAction(sequence(moveTo(0, stage.getHeight()), moveTo(0, 0, .5f))); // coming in from top animation
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        skin.dispose();
    }

}