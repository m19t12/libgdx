package com.mygdx.game.screens;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.RopeJointDef;
import com.mygdx.game.InputController;
import com.badlogic.gdx.utils.Array;



/**
 * Created by Manolis on 30/10/2015.
 */

public class Play2 implements Screen {

    private World world;
    private Box2DDebugRenderer debugRenderer;
    private OrthographicCamera camera;
    private SpriteBatch batch;

    private final float TIMESTEP = 1 / 60f;
    private final int VELOCITYITERATIONS = 8, POSITIONITERETION = 3;

    private Sprite boxSprite;

    private  float speed = 500;
    private Vector2 movement = new Vector2();
    private Body box;

    private Array<Body> tmpBodies = new Array<Body>();

    @Override
    public void show() {


        world = new World(new Vector2(0, -9.81f), true);

        debugRenderer = new Box2DDebugRenderer();

        batch = new SpriteBatch();

        camera = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        camera.position.set(camera.viewportHeight/2,camera.viewportHeight/2,0);

        //Gdx.input.setOnscreenKeyboardVisible(true);

        Gdx.input.setInputProcessor(new InputController(){
            @Override
            public boolean keyDown(int keycode){
                switch (keycode) {
                    case Input.Keys.ESCAPE:
                        ((Game) Gdx.app.getApplicationListener()).setScreen(new LevelMenu());
                        break;
                    case Input.Keys.W:
                        movement.y = speed;
                        break;
                    case Input.Keys.A:
                        movement.x = -speed;
                        break;
                    case Input.Keys.S:
                        movement.y = -speed;
                        break;
                    case Input.Keys.D:
                        movement.x = speed;
                }
                return true;
            }
            @Override
            public boolean keyUp(int keycode){
                switch (keycode) {
                    case Input.Keys.W:
                    case Input.Keys.S:
                        movement.y = 0;
                        break;
                    case Input.Keys.A:
                    case Input.Keys.D:
                        movement.x = 0;
                        break;
                }
                return true;
            }

            @Override
            public boolean scrolled(int amount){
                camera.zoom += amount/25f;
                return true;
            }
        });

        BodyDef bodyDef = new BodyDef();
        FixtureDef fixtureDef = new FixtureDef();

        // Box
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(2.25f, 10);

        // Box Shape
        PolygonShape boxShape = new PolygonShape();
        // half with, half heigh (1 meter , 2 meter)
        boxShape.setAsBox(.5f ,1);

        // Fixture definition
        fixtureDef.shape = boxShape;
        fixtureDef.friction = .75f;
        fixtureDef.restitution = .1f;
        fixtureDef.density = 5;

        box = world.createBody(bodyDef);
        Sprite boxSprite = new Sprite(new Texture("img/luigi_front.png"));
        box.createFixture(fixtureDef);

        boxSprite.setSize(.5f * 2, 1 * 2);
        boxSprite.setOrigin(boxSprite.getWidth() / 2, boxSprite.getHeight() / 2);
        box.setUserData(boxSprite);
        boxShape.dispose();

        // Ball
        // Ball shape
        CircleShape ballShape = new CircleShape();
        ballShape.setPosition(new Vector2(0,1.5f));
        ballShape.setRadius(.5f);

        // fixture definition
        fixtureDef.shape = ballShape;
        fixtureDef.density = 2.5f;
        fixtureDef.friction = .25f;
        fixtureDef.restitution = .75f;

        box.createFixture(fixtureDef);

        ballShape.dispose();

        // Ground
        //body definition
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(0, 0);

        // ground shape
        ChainShape groundShape = new ChainShape();
        groundShape.createChain(new Vector2[]{new Vector2(-50,0), new Vector2(50,0)});

        //fixture definition
        fixtureDef.shape = groundShape;
        fixtureDef.friction = .5f;
        fixtureDef.restitution = 0;

        Body ground = world.createBody(bodyDef);
        ground.createFixture(fixtureDef);

        // other box
        bodyDef.position.y = 7;

        PolygonShape otherBoxShape = new PolygonShape();
        otherBoxShape.setAsBox(.25f,.25f);

        fixtureDef.shape = otherBoxShape;

        Body otherBox = world.createBody(bodyDef);
        otherBox.createFixture(fixtureDef);

        otherBoxShape.dispose();

        // DistanceJoin betwwn other box and box
        DistanceJointDef distanceJointDef = new DistanceJointDef();
        distanceJointDef.bodyA = otherBox;
        distanceJointDef.bodyB = box;
        distanceJointDef.length = 5;

        world.createJoint(distanceJointDef);

        // ropejoint between ground and box
        RopeJointDef ropeJointDef = new RopeJointDef();
        ropeJointDef.bodyA = ground;
        ropeJointDef.bodyB = box;
        ropeJointDef.maxLength = 4;
        ropeJointDef.localAnchorA.set(0,0);
        ropeJointDef.localAnchorB.set(0,0);

        world.createJoint(ropeJointDef);

        groundShape.dispose();
    }

    @Override
    public void render(float delta) {
       // ShaderProgram.pedantic = false;
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        world.step(TIMESTEP, VELOCITYITERATIONS, POSITIONITERETION);

        box.applyForceToCenter(movement, true);
        camera.position.set(box.getPosition().x, box.getPosition().y, 0);
        camera.update();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        world.getBodies(tmpBodies);
        for (Body body : tmpBodies)
            if(body.getUserData() != null && body.getUserData() instanceof Sprite){
                Sprite sprite = (Sprite) body.getUserData();
                sprite.setPosition(body.getPosition().x - sprite.getWidth()/2,body.getPosition().y-sprite.getHeight()/2);
                sprite.setRotation(body.getAngle()* MathUtils.radiansToDegrees);
                sprite.draw(batch);
            }
        batch.end();

        debugRenderer.render(world, camera.combined);
    }

    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width/25;
        camera.viewportHeight = height /25;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        world.dispose();
        boxSprite.getTexture().dispose();
        debugRenderer.dispose();
    }
}
