package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

import java.util.Comparator;

/**
 * Created by Manolis on 30/11/2015.
 */
public class SortedSpriteTutorial implements Screen {

    public static class SortableSprite extends Sprite implements Comparable<SortableSprite>{

        private float z;

        public SortableSprite(Texture texture, float z) {
            super(texture);
            this.z = z;
        }

        @Override
        public int compareTo(SortableSprite o) {
            return (z - o.z) > 0 ? 1 : -1;
        }

        public float getZ() {
            return z;
        }

        public void setZ(float z) {
            this.z = z;
        }
    }

    private SpriteBatch batch = new SpriteBatch();

    private Array<SortableSprite> sprites = new Array<SortableSprite>();

/*    private Comparator<SortableSprite> comparator = new Comparator<SortableSprite>() {
        @Override
        public int compare(SortableSprite s1, SortableSprite s2) {
            return (s1.z - s2.z) > 0 ? 1 : -1;
        }
    };*/

    @Override
    public void show() {

        for (int i = 0; i < 25; i++) {
            SortableSprite sprite = new SortableSprite(new Texture("img/drop.png"), -i);
            sprite.setSize(50, 50);
            sprite.setPosition(i * 2, i * 2);
            sprites.add(sprite);
        }
        sprites.shuffle();

        //sprites.sort(comparator);
        sprites.sort();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        batch.begin();
        for (SortableSprite sprite : sprites)
            sprite.draw(batch);
        batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        batch.dispose();
    }
}
