package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.entites.AISprite;

/**
 * Created by Manolis on 28/11/2015.
 */
public class WaypointsTutorial implements Screen {

    private ShapeRenderer sr;
    private SpriteBatch batch;
    private Array<AISprite> aiSprites;
    private Sprite sprite;

    @Override
    public void show() {
        Gdx.gl.glClearColor(0, 0, 0, 1);

        sr = new ShapeRenderer();
        batch = new SpriteBatch();

        sprite = new Sprite(new Texture("img/drop.png"));
        sprite.setSize(50, 50);
        sprite.setOrigin(0, 0);

        aiSprites = new Array<AISprite>();
        for (int i = 0; i < MathUtils.random(5, 10); i++) {
            AISprite aiSprite = new AISprite(sprite, getRandomPath());
            aiSprite.setPosition(MathUtils.random(0, Gdx.graphics.getWidth()), MathUtils.random(0, Gdx.graphics.getHeight()));
            aiSprites.add(aiSprite);
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);

        batch.begin();
        for (AISprite aiSprite : aiSprites)
            aiSprite.draw(batch);
        batch.end();

        // using the ShapeRenderer properly, thanks to thebiKi (http://www.youtube.com/user/thebiKi)
        sr.setColor(Color.WHITE);
        sr.begin(ShapeRenderer.ShapeType.Line);
        for (AISprite aiSprite : aiSprites) {
            Vector2 previous = aiSprite.getPath().first();
            for (Vector2 waypoint : aiSprite.getPath()) {
                sr.line(previous, waypoint);
                previous = waypoint;
            }
        }
        sr.end();

        sr.begin(ShapeRenderer.ShapeType.Filled);
        for (AISprite aiSprite : aiSprites)
            for (Vector2 waypoint : aiSprite.getPath())
                sr.circle(waypoint.x, waypoint.y, 5);
        sr.end();

        sr.setColor(Color.CYAN);
        sr.begin(ShapeRenderer.ShapeType.Line);
        for (AISprite aiSprite : aiSprites)
            sr.line(new Vector2(aiSprite.getX(), aiSprite.getY()), aiSprite.getPath().get(aiSprite.getWaypoint()));
        sr.end();
    }

    private Array<Vector2> getRandomPath() {
        Array<Vector2> path = new Array<Vector2>();
        for (int i = 0; i < MathUtils.random(5, 10); i++) {
            path.add(new Vector2(MathUtils.random(0, Gdx.graphics.getWidth()), MathUtils.random(0, Gdx.graphics.getHeight())));
        }
        return path;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        batch.dispose();
        sr.dispose();
    }
}
